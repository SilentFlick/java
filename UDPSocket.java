import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class UDPSocket implements AutoCloseable
{
    /* Wenn eine Klasse diese Schnittstelle implementiert, dann können deren
     * Objekte in einer Try-with-resources-Anweisung verwendet werden.
     *
     * try(UDPSocket newSocket = new UDPSocket();
     *     UDPSocket newSocket2 = new UDPSocket())
     * {
     *     newSocket.send();
     *     newSocket2.send();
     *     ...
     * }
     *
     * Beim Verlassen des Try-Blocks in jedem Fall noch die Methode close auf das
     * newSocket-Objekt angewendet wird. Catch- oder Finally-Block ist nicht
     * unbedingt notwendig.
     */
    protected DatagramSocket socket;
    protected InetAddress address;
    protected int port;

    public UDPSocket() throws SocketException
    {
        this(new DatagramSocket());
    }

    public UDPSocket(int port) throws SocketException
    {
        this(new DatagramSocket(port));
    }

    public UDPSocket(DatagramSocket socket)
    {
        this.socket = socket;
    }

    public void send(String s, InetAddress rcvrAddress, int rcvrPort)
        throws IOException
    {
        byte[] outBuffer = s.getBytes();
        DatagramPacket outPacket = new DatagramPacket(outBuffer, outBuffer.length,
                                                      rcvrAddress, rcvrPort);
        socket.send(outPacket);
    }

    public String receive(int maxBytes) throws IOException
    {
        byte[] inBuffer = new byte[maxBytes];
        DatagramPacket  inPacket = new DatagramPacket(inBuffer, inBuffer.length);
        socket.receive(inPacket);
        address = inPacket.getAddress();
        port = inPacket.getPort();
        return new String(inBuffer, 0, inPacket.getLength());
    }

    public void reply(String s) throws IOException
    {
        if(address == null)
            {
                throw new IOException("no one to reply");
            }
        send(s, address, port);
    }

    public InetAddress getSenderAddress()
    {
        return address;
    }

    public int getSenderPort()
    {
        return port;
    }

    public void setTimeout(int timeout) throws SocketException
    {
        socket.setSoTimeout(timeout);
    }

    public void close()
    {
        socket.close();
    }
}
