import java.util.*;
public class Commands {
  /**
   * InnerCommands
   */
  public interface InnerCommands {
    public void run();
  }
  public void A() { System.out.print("A"); }
  public void B() { System.out.print("B"); }
  public void run() {
    List<InnerCommands> arr = new ArrayList<>();
    arr.add(new InnerCommands() {
      @Override
      public void run() {
        A();
      }
    });
    arr.add(new InnerCommands() {
      @Override
      public void run() {
        B();
      }
    });
    arr.get(0).run();
    arr.get(1).run();
  }
  public static void main(String[] args) {
    Commands cmd = new Commands();
    cmd.run();
  }
}
