import java.util.ArrayList;
import java.util.List;

public class MVector
{
    public static void main(String[] args)
    {
        List<InnerMVector> commands = new ArrayList<InnerMVector>();
        commands.add(new A());
        commands.add(new B());
        for (InnerMVector command : commands) {
            command.run();
        }
        commands.get(0).run();
        commands.set(0, new B());
        commands.get(0).run();
    }
}
