import java.net.ServerSocket;

class Counter {
    private int counter;

    public synchronized int increment() {
        counter++;
        return counter;
    }

    public synchronized int reset() {
        counter = 0;
        return counter;
    }

    public synchronized int getCounter() {
        return counter;
    }
}

public class ParallelStaticServer {
    private static final int NUMBER_OF_SLAVES = 20;

    public static void main(String[] args) {
        ServerSocket serverSocket = null;
        Counter counter = new Counter();
        try {
            serverSocket = new ServerSocket(1250);
        } catch (Exception e) {
            System.out.println(e);
            return;
        }
        for (int i = 0; i < NUMBER_OF_SLAVES; i++) {
            Thread t = new StaticSlave(serverSocket, counter);
            t.start();
        }
    }
}

class StaticSlave extends Thread {
    private ServerSocket serverSocket;
    private Counter counter;

    public StaticSlave(ServerSocket serverSocket, Counter counter) {
        this.serverSocket = serverSocket;
        this.counter = counter;
    }

    public void run() {
        while (true) {
            try (TCPSocket tcpSocket = new TCPSocket(serverSocket.accept())) {
                while (true) {
                    String request = tcpSocket.receiveLine();
                    int result;
                    if (request != null) {
                        if (request.equals("increment")) {
                            result = counter.increment();
                        } else if (request.equals("reset")) {
                            result = counter.reset();
                        } else {
                            result = counter.getCounter();
                        }
                        tcpSocket.sendLine("" + result);
                    } else {
                        break;
                    }
                }
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }
}
