import java.net.ServerSocket;

public class ParallelDynamicServer {
    public static void main(String[] args) {
        try (ServerSocket serverSocket = new ServerSocket(1250)) {
            while (true) {
                try {
                    TCPSocket tcpSocket = new TCPSocket(serverSocket.accept());
                    new Slave(tcpSocket);
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}

class Slave extends Thread {
    private TCPSocket socket;

    public Slave(TCPSocket socket) {
        this.socket = socket;
        this.start();
    }

    public void run() {
        try (TCPSocket s = socket) {
            while (true) {
                String request = s.receiveLine();
                if (request != null) {
                    try {
                        int secs = Integer.parseInt(request);
                        Thread.sleep(secs);
                    } catch (InterruptedException ie) {
                        System.out.println(ie);
                    }
                    s.sendLine(request);
                } else {
                    break;
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
