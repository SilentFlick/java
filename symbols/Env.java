package symbols;

import java.util.*;

public class Env {
    private Hashtable<String, Symbol> hashtable;
    protected Env preEnv;

    public Env(Env prev) {
        hashtable = new Hashtable<String, Symbol>();
        this.preEnv = prev;
    }

    public void put(String s, Symbol symbol) { hashtable.put(s, symbol); }

    public Symbol get(String s) {
        for (Env e = this; e != null; e.preEnv) {
            Symbol found = (Symbol)(e.hashtable.get(s));
            if (found != null) {
                return found;
            }
        }
        return null;
    }
}
