package lexer;

import java.io.IOException;
import java.util.Hashtable;

public class Lexer {
    public int line = 1;
    private char peek = ' ';
    private Hashtable<String, World> worlds = new Hashtable<String, World>();

    private void reserve(World w) { worlds.put(w.lexeme, w); }

    public Lexer() {
        reserve(new World(Tag.TRUE, "true"));
        reserve(new World(Tag.FALSE, "false"));
    }

    public Token scan() throws IOException {
        for (;; peek = (char)System.in.read()) {
            if (peek == ' ' || peek == '\t') {
                continue;
            } else if (peek == '\n') {
                line = line + 1;
            } else {
                break;
            }
        }
        if (Character.isDigit(peek)) {
            int v = 0;
            do {
                v = 10 * v + Character.digit(peek, 10);
                peek = (char)System.in.read();
            } while (Character.isDigit(peek));
            return new Num(v);
        }
        if (Character.isLetter(peek)) {
            StringBuffer buffer = new StringBuffer();
            do {
                buffer.append(peek);
                peek = (char)System.in.read();
            } while (Character.isLetterOrDigit(peek));
            String s = buffer.toString();
            World w = (World)worlds.get(s);
            if (w != null) {
                return w;
            }
            w = new World(Tag.ID, s);
            worlds.put(s, w);
            return w;
        }
        Token t = new Token(peek);
        peek = ' ';
        return t;
    }
}
