package lexer;

public class World extends Token {
    public final String lexeme;
    public World(int t, String s) {
        super(t);
        lexeme = new String(s);
    }
}
